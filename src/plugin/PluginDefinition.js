import React from 'react';
import Accessible from '@material-ui/icons/Accessible';
import InputTextField from './PluginComponent';

const MyPlugin = {
    Component: InputTextField,
    IconComponent: <Accessible />,
    name: 'example/content/input-text-field',
    version: '0.0.1',
    text: 'My Text Plugin'
  }

export default MyPlugin;