import React, { Component } from 'react';

import Editor, { Editable, createEmptyState } from 'ory-editor-core';
import 'ory-editor-core/lib/index.css';

// The rich text area plugin
import slate from 'ory-editor-plugins-slate';
import 'ory-editor-plugins-slate/lib/index.css';

// The spacer plugin
import spacer from 'ory-editor-plugins-spacer';
import 'ory-editor-plugins-spacer/lib/index.css';

// The image plugin
import { imagePlugin } from 'ory-editor-plugins-image';
import 'ory-editor-plugins-image/lib/index.css';

// The video plugin
import video from 'ory-editor-plugins-video';
import 'ory-editor-plugins-video/lib/index.css';

// The background plugin
import background, { COLOR_MODE_FLAG, IMAGE_MODE_FLAG, GRADIENT_MODE_FLAG } from 'ory-editor-plugins-background';
import 'ory-editor-plugins-background/lib/index.css';

// The html5-video plugin
import html5video from 'ory-editor-plugins-html5-video';
import 'ory-editor-plugins-html5-video/lib/index.css';

// The native handler plugin
import native from 'ory-editor-plugins-default-native';

// The divider plugin
import divider from 'ory-editor-plugins-divider';

// The default ui components
import { Trash, DisplayModeToggle, Toolbar } from 'ory-editor-ui';
import 'ory-editor-ui/lib/index.css';

import MyPlugin from './plugin/PluginDefinition';

const fakeImageUploadService = (defaultUrl) => { return {url: defaultUrl} };

const editable = createEmptyState();

const plugins = {
  content: [MyPlugin,slate(), spacer, imagePlugin({ imageUpload: fakeImageUploadService() }), video, divider, html5video],
  layout: [
    background({
      defaultPlugin: slate(),
      enabledModes: COLOR_MODE_FLAG | IMAGE_MODE_FLAG | GRADIENT_MODE_FLAG
    }),
  ],

  // If you pass the native key the editor will be able to handle native drag and drop events (such as links, text, etc).
  // The native plugin will then be responsible to properly display the data which was dropped onto the editor.
  native
}

const editor = new Editor({
  plugins: plugins,
  editables: [editable],
  defaultPlugin: slate(),
});

class App extends Component {
  render() {
    return (
      <div className="editable-area">
        <Editable
          editor={editor}
          id={editable.id}
        />
        <Trash editor={editor} />
        <DisplayModeToggle editor={editor} />
        <Toolbar editor={editor} />
      </div>
    );
  }
}

export default App;

